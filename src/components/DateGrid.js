import React from 'react';
import PropTypes from 'prop-types';
import { namedDays, getGridDates } from '../dateHelper';

const DateGrid = ({ month, year }) => {
  const allDays = getGridDates(month, year);

  return (
    <div className="cal__dategrid">
      {
        allDays.map((date, i) => (
          <div className={`cal__day ${date.overflow ? 'cal__day--overflow' : ''}`}>
            {(i < 7) && (<span className="cal__day-name">{namedDays[i]}</span>)}
            <span className="cal__day-text">{date.day}</span>
          </div>
        ))
      }
    </div>
  );
};

DateGrid.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default DateGrid;
