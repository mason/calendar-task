export function updateURL(month, year) {
  window.location.hash = `${month},${year}`;
}

export function getDateFromURL() {
  const hashArray = window.location.hash.substring(1).split(',');
  if (hashArray.length !== 2) {
    return false;
  }

  return {
    month: parseInt(hashArray[0], 10),
    year: parseInt(hashArray[1], 10),
  };
}
