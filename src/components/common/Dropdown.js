import React from 'react';
import PropTypes from 'prop-types';

const Dropdown = ({ items, onChange, activeValue, name, className }) => (
  <select onChange={onChange} value={activeValue} className={className} name={name}>
    { items.map(item => (<option key={item.value} value={item.value}>{item.text}</option>)) }
  </select>
);

Dropdown.defaultProps = {
  activeValue: '',
  className: '',
  name: '',
};

Dropdown.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChange: PropTypes.func.isRequired,
  activeValue: PropTypes.number,
  className: PropTypes.string,
  name: PropTypes.string,
};

export default Dropdown;
