import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropdown from './common/Dropdown';
import { namedMonths, getYearList } from '../dateHelper';


class NavBar extends Component {
  constructor(props) {
    super(props);
    this.years = getYearList(1960, 100);
    this.changeDate = this.changeDate.bind(this);
    this.previousMonth = this.previousMonth.bind(this);
    this.nextMonth = this.nextMonth.bind(this);
  }

  changeDate(e) {
    e.preventDefault();
    this.props.changeDate(e.target.name, parseInt(e.target.value, 10));
  }

  previousMonth() {
    this.props.shiftMonth(-1);
  }

  nextMonth() {
    this.props.shiftMonth(1);
  }

  render() {
    const { month, year, changeToToday } = this.props;

    return (
      <div className="cal__navbar">
        <button
          className="cal__navbar_btn cal__navbar_btn--prev"
          title="Previous"
          onClick={this.previousMonth}
        >&lt;
        </button>
        <button
          className="cal__navbar_btn cal__navbar_btn--next"
          title="Next"
          onClick={this.nextMonth}
        >&gt;
        </button>

        <Dropdown
          className="cal__nav-select cal__navbar__months"
          activeValue={month}
          items={namedMonths.map((monthText, i) => ({ value: i, text: monthText }))}
          name="month"
          onChange={this.changeDate}
        />

        <Dropdown
          className="cal__nav-select cal__navbar__years"
          activeValue={year}
          items={this.years}
          name="year"
          onChange={this.changeDate}
        />

        <button
          className="cal__navbar_btn cal__navbar_btn--today"
          title="Today"
          onClick={changeToToday}
        >Today
        </button>
      </div>);
  }
}

NavBar.propTypes = {
  changeDate: PropTypes.func.isRequired,
  shiftMonth: PropTypes.func.isRequired,
  changeToToday: PropTypes.func.isRequired,
  year: PropTypes.number.isRequired,
  month: PropTypes.number.isRequired,
};

export default NavBar;
