module.exports = {
    "extends": "airbnb",
    env: {
        browser: true,
    },
    parser: "babel-eslint",
    plugins: ["react"],
    rules: {
        "react/jsx-filename-extension": [0],
        "jsx-a11y/label-has-for": [0],
        "jsx-a11y/href-no-hash": [0],
        "react/jsx-tag-spacing": 0,
    },
};