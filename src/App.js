import React, { Component } from 'react';
import NavBar from './components/NavBar';
import DateGrid from './components/DateGrid';
import { getDateFromURL, updateURL } from './store';
import './styles/App.css';


class App extends Component {
  constructor(props) {
    super(props);

    const urlDate = getDateFromURL();
    const now = new Date();

    this.state = {
      month: (urlDate) ? urlDate.month : now.getMonth(),
      year: (urlDate) ? urlDate.year : now.getFullYear(),
    };


    this.changeDate = this.changeDate.bind(this);
    this.shiftMonth = this.shiftMonth.bind(this);
    this.changeToToday = this.changeToToday.bind(this);
  }

  changeDate(unit, value) {
    this.setState({ [unit]: value }, () => updateURL(this.state.month, this.state.year));
  }

  shiftMonth(direction) {
    let month = this.state.month + direction;
    let { year } = this.state;

    if (month < 0) {
      month = 11;
      year -= 1;
    }

    if (month > 11) {
      month = 0;
      year += 1;
    }

    this.setState({ month, year }, () => updateURL(this.state.month, this.state.year));
  }

  changeToToday() {
    const now = new Date();
    this.setState({
      month: now.getMonth(),
      year: now.getFullYear(),
    }, () => updateURL(this.state.month, this.state.year));
  }

  render() {
    return (
      <div className="App">
        <NavBar
          {...this.state}
          changeDate={this.changeDate}
          shiftMonth={this.shiftMonth}
          changeToToday={this.changeToToday}
        />
        <DateGrid {...this.state} />
      </div>
    );
  }
}


export default App;
