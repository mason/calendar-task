export const namedDays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

export const namedMonths = ['January', 'February', 'March', 'April', 'May',
  'June', 'July', 'August', 'September', 'October', 'November', 'December'];

export function getYearList(startYear, yearCount) {
  const years = Array(yearCount).fill();
  return years.map((val, i) => ({ value: startYear + i, text: startYear + i }));
}

export function daysInMonth(month, year) {
  return new Date(year, month + 1, 0).getDate();
}

export function getGridDates(month, year) {
  const newDate = new Date(year, month, 1);
  // Shifted to mon - sun 0 - 6
  const startDay = (newDate.getDay() - 1 < 0) ? 6 : newDate.getDay() - 1;

  // Calc number of previous month overflow days need and get dates
  const daysInPrevMonth = daysInMonth(month - 1, year);
  const preOverflow = Array(startDay).fill().map((val, i) => ({
    day: daysInPrevMonth - i, overflow: true,
  }));
  preOverflow.reverse();

  // Create array of current month's days
  const daysCount = daysInMonth(month, year);
  const currentDays = Array(daysCount).fill().map((val, i) => ({
    day: i + 1, overflow: false,
  }));

  // Calc number of rows an remaining day to fill all rows
  const gridCount = Math.ceil((startDay + daysCount) / 7) * 7;
  const nextOverflowDayCount = gridCount - (daysCount + startDay);
  const nextOverflowDays = Array(nextOverflowDayCount).fill().map((val, i) => ({
    day: i + 1, overflow: true,
  }));

  // Concat all prev, current and next days into one array
  return [...preOverflow, ...currentDays, ...nextOverflowDays];
}
